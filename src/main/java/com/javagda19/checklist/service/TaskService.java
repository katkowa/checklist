package com.javagda19.checklist.service;

import com.javagda19.checklist.model.AppUser;
import com.javagda19.checklist.model.Task;
import com.javagda19.checklist.repository.AppUserRepository;
import com.javagda19.checklist.repository.TaskRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TaskService {
    private TaskRepository taskRepository;
    private AppUserRepository appUserRepository;

    public TaskService(final TaskRepository taskRepository, final AppUserRepository appUserRepository) {
        this.taskRepository = taskRepository;
        this.appUserRepository = appUserRepository;
    }

    public List<Task> getAllTask() {
        return taskRepository.findAll();
    }

    @Transactional
    public List<Task> getUserTasks(final String userName, final boolean activeOnly) {
        return taskRepository.findByUserEmail(userName);

    }

    public List<Task> getUserActiveTasks(final String userName , final boolean isDone) {
        return taskRepository.findByUserEmailAndIsDone(userName, isDone);
    }

    public void changeStatus(final Long taskId, final boolean isDone) {
        Optional<Task> taskOptional = taskRepository.findById(taskId);
        if (taskOptional.isPresent()) {
            Task task = taskOptional.get();
            task.setDone(isDone);
            taskRepository.save(task);
        }
    }

    public void save(final Task task, final String username) {
        Optional<AppUser> userOptional = appUserRepository.findByEmail(username);
        if (userOptional.isPresent()) {
            task.setUser(userOptional.get());
        }
        taskRepository.save(task);
    }

    public void delete(final Long taskId) {
        taskRepository.deleteById(taskId);
    }
}
