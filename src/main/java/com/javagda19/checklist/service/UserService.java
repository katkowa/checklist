package com.javagda19.checklist.service;

import com.javagda19.checklist.model.AppUser;

import java.util.List;

public interface UserService {
    void registerUser(String username, String password, String passwordConfirm);

    List<AppUser> getAllUsers();

    void deleteUserById(Long userId);

    void makeAdmin(Long userId);
}
