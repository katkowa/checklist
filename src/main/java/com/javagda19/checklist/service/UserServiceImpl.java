package com.javagda19.checklist.service;

import com.javagda19.checklist.exception.InvalidEmailException;
import com.javagda19.checklist.exception.PasswordDoNotMatchException;
import com.javagda19.checklist.model.AppUser;
import com.javagda19.checklist.model.Task;
import com.javagda19.checklist.model.UserRole;
import com.javagda19.checklist.repository.AppUserRepository;
import com.javagda19.checklist.repository.TaskRepository;
import com.javagda19.checklist.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {
    private AppUserRepository appUserRepository;
    private UserRoleRepository userRoleRepository;
    private BCryptPasswordEncoder passwordEncoder;
    private UserRoleService userRoleService;
    private TaskRepository taskRepository;

    @Autowired
    public UserServiceImpl(AppUserRepository appUserRepository, final UserRoleRepository userRoleRepository, BCryptPasswordEncoder passwordEncoder, UserRoleService userRoleService, final TaskRepository taskRepository) {
        this.appUserRepository = appUserRepository;
        this.userRoleRepository = userRoleRepository;
        this.passwordEncoder = passwordEncoder;
        this.userRoleService = userRoleService;
        this.taskRepository = taskRepository;
    }

    @Override
    public void registerUser(String username, String password, String passwordConfirm) {
        if(!password.equals(passwordConfirm)){
            throw new PasswordDoNotMatchException("Password and Password Confirm do not match.");
        }
        if(password.length() <= 3){
            throw new PasswordDoNotMatchException("Password must be at least 4 characters.");
        }
        if (appUserRepository.findAppUserByEmail(username).size() > 0) {
            throw new InvalidEmailException("User with this email already exists");
        }
        AppUser appUser = new AppUser();
        appUser.setEmail(username);
        appUser.setPassword(passwordEncoder.encode(password));

        appUser.setRoles(userRoleService.getDefaultUserRoles());

        appUserRepository.save(appUser);
    }

    @Override
    public List<AppUser> getAllUsers() {
        return appUserRepository.findAll();
    }

    @Override
    @Transactional
    public void deleteUserById(final Long userId) {
        List<Task> tasks = taskRepository.findByUserId(userId);
        taskRepository.deleteAll(tasks);
        appUserRepository.deleteById(userId);
    }

    @Override
    public void makeAdmin(final Long userId) {
        Optional<AppUser> userOptional = appUserRepository.findById(userId);
        if (userOptional.isPresent()) {
            AppUser user = userOptional.get();
            Set<UserRole> roles = user.getRoles();
            roles.add(userRoleRepository.findByName("ADMIN"));
            user.setRoles(roles);
            appUserRepository.save(user);
        }
    }
}
