package com.javagda19.checklist.service;

import com.javagda19.checklist.model.UserRole;

import java.util.Set;

public interface UserRoleService {
    Set<UserRole> getDefaultUserRoles();
}
