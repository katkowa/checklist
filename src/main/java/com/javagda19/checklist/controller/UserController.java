package com.javagda19.checklist.controller;

import com.javagda19.checklist.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

@Controller
@RequestMapping("/user")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(final UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/register")
    public String getRegisterForm() {
        return "userRegister"; // załaduj widok userRegister (formularz)
    }

    @PostMapping("/register")
    public String submitRegisterForm(@RequestParam(name = "username") String username,
                                     @RequestParam(name = "password") String password,
                                     @RequestParam(name = "password-confirm") String passwordConfirm){
        userService.registerUser(username, password, passwordConfirm); // zapisz użytkownika w bazie

        return "redirect:/login";
    }
}
