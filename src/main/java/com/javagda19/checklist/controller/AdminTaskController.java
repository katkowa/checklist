package com.javagda19.checklist.controller;

import com.javagda19.checklist.service.TaskService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/admin/task")
public class AdminTaskController {

    private TaskService taskService;

    public AdminTaskController(final TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/list")
    public String listAllTask(Model model) {
        model.addAttribute("taskList", taskService.getAllTask());
        return "taskList";
    }
}
