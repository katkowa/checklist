package com.javagda19.checklist.controller;

import com.javagda19.checklist.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.websocket.server.PathParam;

@Controller
@RequestMapping("/admin/user")
public class AdminUserController {

    private final UserService userService;

    @Autowired
    public AdminUserController(final UserService userService) {
        this.userService = userService;
    }

    @GetMapping(path ="/list")
    public String listUsers(Model model) {
        model.addAttribute("userList", userService.getAllUsers());
        return "userList";
    }

    @PostMapping("/delete")
    public String deleteUser(@PathParam(value = "userId") Long userId) {
        userService.deleteUserById(userId);
        return "redirect:/admin/user/list";
    }


    @PostMapping("/make-admin")
    public String makeAdmin(@RequestParam Long userId) {
        userService.makeAdmin(userId);
        return "redirect:/admin/user/list";
    }


}
