package com.javagda19.checklist.controller;

import com.javagda19.checklist.model.Task;
import com.javagda19.checklist.service.TaskService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping(path = "/task")
public class TaskController {
    private TaskService taskService;

    public TaskController(final TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/list")
    public String getTaskList(Model model) {
        model.addAttribute("taskList", getActiveUserTasks());
        model.addAttribute("addingAvailable", true);
        return "taskList";
    }

    private List<Task> getActiveUserTasks() {
        return taskService.getUserTasks(getCurrentUserName(), false);
    }

    private String getCurrentUserName() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getName();
    }

    @GetMapping("/active")
    public String getActiveTaskList(Model model) {
        model.addAttribute("taskList", getTaskList(false));
        model.addAttribute("addingAvailable", false);
        return "taskList";
    }

    @GetMapping("/inactive")
    public String getInactiveTaskList(Model model) {
        model.addAttribute("taskList", getTaskList(true));
        model.addAttribute("addingAvailable", false);
        return "taskList";
    }

    private List<Task> getTaskList(boolean isDone) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentUserName = authentication.getName();
        return taskService.getUserActiveTasks(currentUserName, isDone);
    }

    @PostMapping("/make-done")
    public String makeDone(@RequestParam Long taskId, HttpServletRequest request) {
        taskService.changeStatus(taskId, true);
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }

    @PostMapping("/make-undone")
    public String makeUndone(@RequestParam Long taskId, HttpServletRequest request) {
        taskService.changeStatus(taskId, false);
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }

    @PostMapping("/add")
    public String addNewTask(Model model) {
        model.addAttribute("newTask", new Task());
        return "taskAdd";
    }

    @PostMapping("/save")
    public String saveNewTask(Task task) {
        taskService.save(task, getCurrentUserName());
        return "redirect:/task/list";
    }

    @PostMapping("delete")
    public String deleteTask(@RequestParam Long taskId, HttpServletRequest request) {
        taskService.delete(taskId);
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }
}
