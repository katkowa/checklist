package com.javagda19.checklist.configuration;

import com.javagda19.checklist.model.AppUser;
import com.javagda19.checklist.model.Task;
import com.javagda19.checklist.model.UserRole;
import com.javagda19.checklist.repository.AppUserRepository;
import com.javagda19.checklist.repository.TaskRepository;
import com.javagda19.checklist.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Component
public class DataInitializer implements ApplicationListener<ContextRefreshedEvent> {
    private final UserRoleRepository userRoleRepository;
    private final AppUserRepository appUserRepository;
    private final PasswordEncoder passwordEncoder;
    private final TaskRepository taskRepository;

    @Autowired
    public DataInitializer(final UserRoleRepository userRoleRepository, final AppUserRepository appUserRepository, final PasswordEncoder passwordEncoder, final TaskRepository taskRepository) {
        this.userRoleRepository = userRoleRepository;
        this.appUserRepository = appUserRepository;
        this.passwordEncoder = passwordEncoder;
        this.taskRepository = taskRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        createRoleIfNotExist("ADMIN");
        createRoleIfNotExist("USER");

        createUserWithRoleIfNotExist("admin", "admin", "ADMIN", "USER");
        createUserWithRoleIfNotExist("user", "user", "USER");

        taskRepository.save(new Task(null, "Sweep the desert", true, appUserRepository.findByEmail("admin").get()));
        taskRepository.save(new Task(null, "Do great 'TO DO' application", false, appUserRepository.findByEmail("admin").get()));
        taskRepository.save(new Task(null, "Travel the whole world", false, appUserRepository.findByEmail("user").get()));
    }

    private void createUserWithRoleIfNotExist(String username, String password, String... roles) {
        if (!appUserRepository.existsByEmail(username)) {
            AppUser appUser = new AppUser();
            appUser.setEmail(username);
            appUser.setPassword(passwordEncoder.encode(password));

            appUser.setRoles(new HashSet<>(findRoles(roles)));

            appUserRepository.save(appUser);
        }
    }

    private List<UserRole> findRoles(String[] roles) {
        List<UserRole> userRoles = new ArrayList<>();

        for (String role : roles) {
            userRoles.add(userRoleRepository.findByName(role));
        }
        return userRoles;
    }

    private void createRoleIfNotExist(String roleName) {
        if (!userRoleRepository.existsByName(roleName)) {
            UserRole role = new UserRole();
            role.setName(roleName);

            userRoleRepository.save(role);
        }
    }


}
