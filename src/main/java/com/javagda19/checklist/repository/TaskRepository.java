package com.javagda19.checklist.repository;

import com.javagda19.checklist.model.AppUser;
import com.javagda19.checklist.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
    List<Task> findByUserEmail(String email);

    List<Task> findByUserEmailAndIsDone(String email, boolean isDone);

    List<Task> findByUserId(Long userId);

}
