package com.javagda19.checklist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Zadanie:
 * Sklonuj aplikację "spring security demo", na bazie tej aplikacji stwórz aplikację zadań "todo tasks"/"checklist" - czyli "zadania do zrobienia".Aplikacja powinna mieć stronę:
 * - "/" - index, home page - strona na której wyswietla się informacja z Twoim imieniem i nazwiskiem. Strona powinna zawierać informację co można zrobić w aplikacji.
 * - "/login" - strona logowania
 * - "/logout" - wylogowywanie
 * - "/task/list" - lista zadań (wszystkich) użytkownika zalogowanego (każdy zalogowany użytkownik ma do tej strony dostęp)
 * - "/task/add" - dodawanie zadania przez użytkownika (do swojego konta)
 * (każdy użytkownik może dodać zadanie, oraz oznaczyć je jako wykonane)
 * - "/task/active" - lista zadań (które nie są oznaczone jako wykonane) użytkownika
 * - "/task/inactive" - lista zadań (które nie są oznaczone jako niewykonane) użytkownika
 * - "/admin/task/list" - lista wszystkich zadań wszystkich użytkowników (dostępne tylko dla admina)
 * (admin może usuwać zadania innym użytkownikom, oraz oznaczać je jako wykonane i *niewykonane* - użytkownik nie może zmienić statusu tasku na niewykonany
 * - "/admin/user/list" - lista użytkowników (dostępne dla admina) - admin może usuwać użytkowników
 */

@SpringBootApplication
public class SpringDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringDemoApplication.class, args);
    }

}
